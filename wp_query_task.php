<?php 

 /** 
 * WP_Query for odesk job https://www.odesk.com/applications/297423744
 * @author Faruk Garic <faruk.garic@gmail.com> 
 */ 


$ids = get_users(array('role' => 'writer' ,'fields' => 'ID')); //Here we get all ids of users which has "writer" role

// WP_Query arguments
$args = array (
	'post_type'              => 'book',
	'tax_query' => array(
		array(
		'taxonomy' => 'genre',
		'field' => 'slug',
		'terms' => 'fantasy'
		)),
	'author__in' => $ids,
	'meta_key'     => 'year_published',
	'meta_value'   => '1970',
	'meta_compare' => '>'
);

// The Query
$query = new WP_Query( $args );
while ($query->have_posts()) : $query->the_post(); 

	//Show some results

endwhile; 
?>